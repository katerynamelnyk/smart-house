import { createServer } from "miragejs"

import SmartTemperatureSensor from "./models/SmartTemperatureSensor";
import SmartOutlet from "./models/SmartOutlet";
import SmartBulb from "./models/SmartBulb";

import { smart } from './components/MainPage'


export function makeServer({ environment = "development" } = {}) {

    const SmartDevice: smart[] = [
        new SmartBulb('b1lkp785', 'Mi LED Smart Bulb', 'connected', true, 75, 'ee8916'),
        new SmartOutlet('o1gds321', 'Mini Smart Outlet Socket', 'poorConnection', false, 125),
        new SmartTemperatureSensor('tslk142', 'Govee Wi-Fi Thermo-Hygrometer', 'disconnected', 24),
    ];

    setInterval(() => {
        SmartDevice[0].connectionState = 'disconnected';
        SmartDevice[1].connectionState = 'connected';
        SmartDevice[2].connectionState = 'poorConnection';
    }, 7000)

    setInterval(() => {
        SmartDevice[0].connectionState = 'poorConnection';
        SmartDevice[1].connectionState = 'disconnected';
        SmartDevice[2].connectionState = 'connected';
    }, 14000)


    let server = createServer({
        environment,

        routes() {
            this.namespace = "api/v1/devices"

            this.get("/", () => {
                return SmartDevice;
            })

            this.get("/:id", (schema, request) => {
                let id = request.params.id;
                let SmartDeviceDetails = SmartDevice.filter(item => id === item.id)[0];
                return SmartDeviceDetails;
            })
        },
    })

    return server;
}