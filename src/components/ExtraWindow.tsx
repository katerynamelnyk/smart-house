import classes from './ExtraWindow.module.css';

import interact from 'interactjs';
import { useEffect, useState } from 'react';
import { coord } from './MainPage';


const ExtraWindow: React.FC<{
    deviceId: string, onHideExtraWindow: Function, startCoords: coord
}> = (props) => {

    const [position, setPosition] = useState(props.startCoords);
    const [isDragable, setIsDragable] = useState(false);

    const [x, setX] = useState('');
    const [y, setY] = useState('');
    const [width, setWidth] = useState(0);
    const [height, setHeight] = useState(0);
    const [isResizable, setIsResizable] = useState(false);


    useEffect(() => {
        interact('.dragableResizable')
            .draggable({
                listeners: {
                    move(event) {
                        setPosition(prev => ({
                            x: prev.x + event.dx,
                            y: prev.y + event.dy
                        }))
                    },
                }
            })
            .resizable({
                edges: { top: true, left: true, bottom: true, right: true },
                listeners: {
                    move: function (event) {
                        setX(event.target.dataset.x);
                        setY(event.target.dataset.y);

                        setX((parseFloat(x) || 0) + event.deltaRect.left);
                        setY((parseFloat(y) || 0) + event.deltaRect.top);

                        setHeight(event.rect.height);
                        setWidth(event.rect.width);

                        Object.assign(event.target.dataset, { x, y });
                    }
                }
            })
        setIsResizable(true);
        setIsDragable(true);
        return (() => {
            setIsDragable(false);
            setIsResizable(false);
        })
    }, [position, x, y, height, width])


    const backToMainWindowHandler = () => {
        props.onHideExtraWindow(false, { x: position.x, y: position.y });
    }

    const [currentItem, setCurrentItem] = useState({});

    useEffect(() => {
        fetch(`/api/v1/devices/${props.deviceId}`)
            .then((res) => res.json())
            .then((data) => setCurrentItem(prev => data))
    }, [currentItem, props.deviceId])


    return <div className='dragableResizable'
        style={{
            transform: isDragable ? `translate(${position.x}px, ${position.y}px)` :
                isResizable ? `translate(${x}px, ${y}px)` : '',
            width: isResizable ? `${width}px` : '',
            height: isResizable ? `${height}px` : '',
            minWidth: '400px',
            minHeight: '600px'
        }}>
        <div className={classes.modal}>
            {Object.entries(currentItem).map((item, ind) =>
                <div key={ind} className={classes.keyValueWrapper}>
                    <p className={classes.key}>{`${item[0]}:`}</p>
                    <p className={classes.val}>
                        {item[0] === 'powerConsumption' ? `${item[1]}W` :
                            item[0] === 'temperature' ? `${item[1]}°C` :
                                item[0] === 'color' ? `#${item[1]}` : `${item[1]}`}
                    </p>
                </div>
            )}
            <button onClick={backToMainWindowHandler}>BACK</button>
        </div>
    </div>

}

export default ExtraWindow;