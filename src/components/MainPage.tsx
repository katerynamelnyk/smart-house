import classes from './MainPage.module.css';

import { Fragment, useEffect, useState } from 'react';

import SmartBulb from '../models/SmartBulb';
import SmartOutlet from '../models/SmartOutlet';
import SmartTemperatureSensor from '../models/SmartTemperatureSensor';

import BlockWithDevice from './BlockWithDevice';
import ExtraWindow from './ExtraWindow';

export type coord = { x: number, y: number };
export type smart = SmartBulb | SmartOutlet | SmartTemperatureSensor;



const MainPage = () => {

    let [data, setData] = useState([]);

    useEffect(() => {
        fetch("/api/v1/devices")
            .then((res) => res.json())
            .then((data) => setData(data))
    }, [])

    useEffect(() => {
        let timerId = setTimeout(function run() {
            fetch("/api/v1/devices")
                .then((res) => res.json())
                .then((data) => setData(data))
            setTimeout(run, 7000);
        }, 7000);
        return () => clearInterval(timerId);
    }, [data])


    const [isShow, setIsShow] = useState(false);

    const [currentDeviceId, setCurrentDeviceId] = useState('');

    const showExtraWindowHandler = (isShowWindow: boolean, currentItemId: string) => {
        setIsShow(isShowWindow);
        setCurrentDeviceId(currentItemId);
    }

    let width = (window.visualViewport.width - 400) / 2;
    let height = (window.visualViewport.height + 600) / 2;

    const [currentCoords, setCurrentCoords] = useState({ x: width, y: -height });

    const showMainWindowHandler = (isHideWindow: boolean, coords: coord) => {
        setIsShow(isHideWindow);
        setCurrentCoords(prev => ({ x: coords.x, y: coords.y }));
    }


    return <Fragment>
        <div className={classes.wrapper}>
            <header className={classes.header}>
                <h2>Smart House</h2>
            </header>
            <div className={classes.mainContent}>
                {data.map((item, ind) =>
                    <BlockWithDevice
                        item={item}
                        key={ind}
                        onShowExtraWindow={showExtraWindowHandler}
                    ></BlockWithDevice>)}
            </div>
        </div>
        {isShow ? <ExtraWindow
            startCoords={currentCoords}
            deviceId={currentDeviceId}
            onHideExtraWindow={showMainWindowHandler}
        ></ExtraWindow> : ''}
    </Fragment>

}

export default MainPage;
