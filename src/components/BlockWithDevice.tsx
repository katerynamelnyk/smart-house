import classes from './BlockWithDevice.module.css';

import { Fragment } from 'react';
import { smart } from './MainPage';


const BlockWithDevice: React.FC<{ item: smart, onShowExtraWindow: Function }> = (props) => {

    const clickHendler = () => {
        props.onShowExtraWindow(true, props.item.id);
    }

    const isDisconnected = props.item.connectionState === 'disconnected';
    const isPoorConnection = props.item.connectionState === 'poorConnection';

    return <Fragment>
        <div className={classes.wrapper}>
            <div className={classes.imageWrapper}>
                {props.item.type === 'bulb' ?
                    <img src={require("./assets/SmartBulb.png")} alt="smart bulb"></img> : ''}
                {props.item.type === 'outlet' ?
                    <img src={require("./assets/SmartOutlet.png")} alt="smart outlet"></img> : ''}
                {props.item.type === 'temperatureSensor' ?
                    <img src={require("./assets/SmartTemperatureSensor.png")} alt="smart temperature sensor">
                    </img> : ''}
            </div>
            <h3 className={classes.type}>{props.item.type}</h3>
            <p className={classes.name}>{props.item.name}</p>
            <div className={classes.buttonWrapper}>
                <button className={classes.mainButton} onClick={clickHendler} value={props.item.id}>DETAILS</button>
            </div>
            <p className=
                {isDisconnected ? classes.connectionRed :
                    isPoorConnection ? classes.connectionYellow : classes.connectionGreen}>
                {props.item.connectionState}
            </p>
        </div>
    </Fragment>
}

export default BlockWithDevice;