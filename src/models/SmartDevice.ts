interface SmartDevice {
    type: 'bulb' | 'outlet' | 'temperatureSensor';
    id: string;
    name: string;
    connectionState: 'connected' | 'disconnected' | 'poorConnection';
}

export default SmartDevice;