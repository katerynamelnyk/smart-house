import SmartDevice from "./SmartDevice";

class SmartOutlet implements SmartDevice {
    type: 'bulb' | 'outlet' | 'temperatureSensor';
    id: string;
    name: string;
    connectionState: 'connected' | 'disconnected' | 'poorConnection';
    isTurnedOn: boolean;
    powerConsumption: number;

    constructor(id: string, name: string,
        connectionState: 'connected' | 'disconnected' | 'poorConnection',
        isTurnedOn: boolean,  powerConsumption: number) {
        this.type = 'outlet';
        this.id = id;
        this.name = name;
        this.connectionState = connectionState;
        this.isTurnedOn = isTurnedOn;
        this.powerConsumption = powerConsumption;
    }
}

export default SmartOutlet;