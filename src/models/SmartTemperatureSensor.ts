import SmartDevice from "./SmartDevice";

class SmartTemperatureSensor implements SmartDevice {
    type: 'bulb' | 'outlet' | 'temperatureSensor';
    id: string;
    name: string;
    connectionState: 'connected' | 'disconnected' | 'poorConnection';
    temperature: number;

    constructor(id: string, name: string,
        connectionState: 'connected' | 'disconnected' | 'poorConnection',
        temperature: number) {
        this.type = 'temperatureSensor';
        this.id = id;
        this.name = name;
        this.connectionState = connectionState;
        this.temperature = temperature;
    }
}

export default SmartTemperatureSensor;