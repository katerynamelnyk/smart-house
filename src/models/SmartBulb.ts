import SmartDevice from "./SmartDevice";

class SmartBulb implements SmartDevice {
    type: 'bulb' | 'outlet' | 'temperatureSensor';
    id: string;
    name: string;
    connectionState: 'connected' | 'disconnected' | 'poorConnection';
    isTurnedOn: boolean;
    brightness: number;
    color: string;

    constructor(id: string, name: string,
        connectionState: 'connected' | 'disconnected' | 'poorConnection',
        isTurnedOn: boolean, brightness: number, color: string) {
        this.type = 'bulb';
        this.id = id;
        this.name = name;
        this.connectionState = connectionState;
        this.isTurnedOn = isTurnedOn;
        if (brightness >= 0 && brightness <= 100) {
            this.brightness = brightness;
        } else {
            this.brightness = 50;
        }
        this.color = color;
    }
}

export default SmartBulb;